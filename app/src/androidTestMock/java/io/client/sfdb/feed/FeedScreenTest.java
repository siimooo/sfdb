package io.client.sfdb.feed;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.client.sfdb.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static java.util.regex.Pattern.matches;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Tests for the add task screen.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class FeedScreenTest {


    /**
     * {@link IntentsTestRule} is an {@link ActivityTestRule} which inits and releases Espresso
     * Intents before and after each test run.
     *
     * <p>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public IntentsTestRule<FeedActivity> mAddTaskIntentsTestRule = new IntentsTestRule<>(FeedActivity.class);

    /**
     * Tests
     */
    @Test
    public void doSearchAndShowResult() {
        // click search from toolbar
        onView(withId(R.id.action_search)).perform(click());
        // type search value for
        onView(withId(R.id.search_src_text)).perform(ViewActions.typeText("batman v superman"));
        // click search icon
        onView(withId(R.id.search_src_text)).perform(ViewActions.pressImeActionButton());
        // Verify film snackbar is shown
//        // Verify empty tasks snackbar is shown
//        String emptyTaskMessageText = getTargetContext().getString(R.string.search_empty_film_message);
//        onView(withText(emptyTaskMessageText)).check(matches(isDisplayed()));
    }

    @Test
    public void doScrollTestAndOOM() {
        // Click item at position 3
        onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
//        onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.scrollToPosition(4));
        onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.actionOnItemAtPosition(0, longClick()));


        for (int i = 0; i < 2000; i+=6) {
            onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.actionOnItemAtPosition(i, scrollTo()));

        }

//        onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.actionOnItemAtPosition(4, scrollTo()));
//        onView(withId(R.id.feed_grid)).perform(RecyclerViewActions.actionOnItemAtPosition(6, scrollTo()));

//        // Check item at position 3 has "Some content"
//        onView(withRecyclerView(R.id.scroll_view).atPosition(3))
//                .check(matches(hasDescendant(withText("Some content"))));
    }




}