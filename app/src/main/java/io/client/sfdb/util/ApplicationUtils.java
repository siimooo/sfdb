package io.client.sfdb.util;

import io.rxjavaomdb.network.PosterClientImpl;

/**
 * User: Simo Ala-Kotila
 * Date: 01/10/16
 * Time: 19:01
 */
public class ApplicationUtils {

    /**
     * set poster image quality
     */
    public static final int IMAGE_RES = PosterClientImpl.POSTER_MEDIUM;


    public static final int POSTER_WIDTH = 300;
    public static final int POSTER_HEIGHT = 445;

}
