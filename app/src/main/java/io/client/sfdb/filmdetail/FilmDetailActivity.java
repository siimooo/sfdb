package io.client.sfdb.filmdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.squareup.picasso.Picasso;

import io.client.sfdb.R;
import io.client.sfdb.feed.FeedFragment;
import io.client.sfdb.util.ActivityUtils;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;

/**
 * User: Simo Ala-Kotila
 * Date: 08/12/16
 * Time: 16:45
 */

public class FilmDetailActivity extends AppCompatActivity{

    public static final String FILM_OBJECT = "FILM_OBJECT";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filmdetail_activity);
        //set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_drawable);//just temp icon

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        OMDbTitleOrId omDbTitleOrId = (OMDbTitleOrId) getIntent().getSerializableExtra(FILM_OBJECT);

        //set up fragment
        FilmDetailFragment filmDetailFragment = (FilmDetailFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (filmDetailFragment == null) {
            // Create the fragment
            filmDetailFragment = filmDetailFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), filmDetailFragment, R.id.contentFrame);
        }
        // Create the presenter
        FilmDetailPresenter presenter = new FilmDetailPresenter(filmDetailFragment, Picasso.with(getApplicationContext()), omDbTitleOrId);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
