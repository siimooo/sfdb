package io.client.sfdb.filmdetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.RequestCreator;

import io.client.sfdb.R;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;

/**
 * User: Simo Ala-Kotila
 * Date: 11/12/16
 * Time: 18:28
 */

public class FilmDetailFragment extends Fragment implements FilmDetailContract.View {

    private FilmDetailContract.Presenter mPresenter;

    public FilmDetailFragment(){
        // Required empty public constructor
    }

    @Override
    public void showPoster(RequestCreator creator) {
        ImageView imageView = (ImageView) getActivity().findViewById(R.id.poster_detail);
        creator.into(imageView);
    }

    @Override
    public void showFilmDetail(OMDbTitleOrId omDbTitleOrId) {
        TextView title = (TextView) getActivity().findViewById(R.id.film_detail_title);
        title.setText(omDbTitleOrId.getTitle());

        TextView description = (TextView) getActivity().findViewById(R.id.film_detail_description);
        description.setText(omDbTitleOrId.getPlot());

        TextView rating = (TextView) getActivity().findViewById(R.id.film_detail_rating);
        rating.setText(omDbTitleOrId.getImdbRating() + " / 10");

        TextView genre = (TextView) getActivity().findViewById(R.id.film_detail_genre);
        genre.setText(omDbTitleOrId.getGenre());
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    public static FilmDetailFragment newInstance(){
        return new FilmDetailFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.filmdetail_fragment, container, false);
        setHasOptionsMenu(true);//enable overflow button (settings)
        return root;
    }

    @Override
    public void setPresenter(FilmDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }
}
