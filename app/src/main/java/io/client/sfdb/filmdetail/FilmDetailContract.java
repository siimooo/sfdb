package io.client.sfdb.filmdetail;

import android.widget.ImageView;

import com.squareup.picasso.RequestCreator;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.BaseView;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;

/**
 * This specifies the contract between the view ({@link }) and the presenter ({@link }).
 *
 * User: Simo Ala-Kotila
 * Date: 08/12/16
 * Time: 16:46
 */

public class FilmDetailContract {

    /**
     *  Main UI for the ({@link })
     */
    interface View extends BaseView<Presenter> {

        void showPoster(RequestCreator creator);

        void showFilmDetail(OMDbTitleOrId omDbTitleOrId);

        boolean isActive();
    }


    /**
     *  Listens to user actions from the UI, updates the UI as required.
     */
    interface Presenter extends BasePresenter{

        /**
         * Load poster for the item.
         *
         * @param url
         *
         */
        void loadPoster(String url);


    }



}
