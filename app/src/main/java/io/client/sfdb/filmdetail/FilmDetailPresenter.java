package io.client.sfdb.filmdetail;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import io.client.sfdb.R;
import io.client.sfdb.util.ApplicationUtils;
import io.rxjavaomdb.network.PosterClientImpl;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.rxjavaomdb.utils.Parser;

/**
 * User: Simo Ala-Kotila
 * Date: 11/12/16
 * Time: 18:28
 */

public class FilmDetailPresenter implements FilmDetailContract.Presenter{

    private final FilmDetailContract.View mFilmDetailView;
    private final Picasso mPicasso;
    private final OMDbTitleOrId mOMDbTitleOrId;

    public FilmDetailPresenter(FilmDetailContract.View filmDetailView, Picasso picasso, OMDbTitleOrId omDbTitleOrId){
        mFilmDetailView = filmDetailView;
        mFilmDetailView.setPresenter(this);
        mPicasso = picasso;
        mOMDbTitleOrId = omDbTitleOrId;
    }

    @Override
    public void subscribe() {
        String url = mOMDbTitleOrId.getPoster();
        String highResURL = Parser.setPosterRes(url, PosterClientImpl.POSTER_HIGH);
        loadPoster(highResURL);
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void loadPoster(String url) {
        RequestCreator creator = mPicasso.load(url);
        mFilmDetailView.showPoster(creator);
        mFilmDetailView.showFilmDetail(mOMDbTitleOrId);
    }
}
