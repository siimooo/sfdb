package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.feed.FeedContract;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 17:33
 */

public class SubscriptionStateLoad implements SubscriptionState2 {

    private int index = 1;

    @Override
    public void doSubscription(BasePresenter holder) {
//        doSubscriptio2n(holder);
    }

    @Override
    public void doSubscription(FeedContract.Presenter presenter) {
        presenter.subscribe(index);
    }


    public void doSubscriptio2n(FeedContract.Presenter presenter) {
        presenter.subscribe(index);
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
