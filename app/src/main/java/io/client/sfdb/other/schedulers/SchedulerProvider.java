package io.client.sfdb.other.schedulers;

import android.support.annotation.NonNull;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Provides different types of schedulers. Taken from https://github.com/googlesamples/android-architecture
 */
public class SchedulerProvider implements BaseSchedulerProvider {

    private SchedulerProvider() { }

    private static class SingletonHolder {
        public final static SchedulerProvider INSTANCE = new SchedulerProvider();
    }

    public static SchedulerProvider getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    @NonNull
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    @NonNull
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    @NonNull
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}
