package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 17:32
 */

public class SubscriptionStateHolder  {

    protected SubscriptionState mState;

    protected final BasePresenter mPresenter;

    public SubscriptionStateHolder(BasePresenter presenter){
        mPresenter = presenter;
    }

    public void setState(SubscriptionState state){
        mState = state;
    }

    public void doSubscription(){
        mState.doSubscription(mPresenter);
    }

    public SubscriptionState getState() {
        return mState;
    }
}
