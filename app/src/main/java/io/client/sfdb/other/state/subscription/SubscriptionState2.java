package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.feed.FeedContract;
import io.client.sfdb.feed.FeedPresenter;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 18:36
 */

public interface SubscriptionState2 extends SubscriptionState{

    void doSubscription(FeedContract.Presenter presenter);
}
