package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 17:21
 */

public interface SubscriptionState {

    void doSubscription(BasePresenter presenter);
}
