package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.feed.FeedContract;
import io.client.sfdb.feed.FeedPresenter;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 18:33
 */

public class SubscriptionStateHolder2 extends SubscriptionStateHolder {

    public SubscriptionStateHolder2(FeedContract.Presenter presenter) {
        super(presenter);
    }
}
