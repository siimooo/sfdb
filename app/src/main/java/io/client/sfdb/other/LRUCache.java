package io.client.sfdb.other;

import android.graphics.Bitmap;
import android.util.LruCache;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: Simo Ala-Kotila
 * Date: 20/10/16
 * Time: 10:32
 */

public class LRUCache {

    private final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    private final int cacheSize = maxMemory / 8;

    private LruCache<String, Bitmap> mMemoryCache;

    private static class SingletonHolder {
        public final static LRUCache INSTANCE = new LRUCache();
    }

    public static LRUCache getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private LRUCache(){
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        synchronized (mMemoryCache){
            if (getBitmapFromMemCache(key) == null) {
                mMemoryCache.put(key, bitmap);
            }

        }
    }

    public boolean isBitmapInMemory(String key){
        synchronized (mMemoryCache) {
            if(getBitmapFromMemCache(key) == null){
                return false;
            }
            return true;
        }
    }

    public void clear(){
        synchronized (mMemoryCache) {
            mMemoryCache.evictAll();
        }
    }


    public Bitmap getBitmapFromMemCache(String key) {
        synchronized (mMemoryCache) {
            return mMemoryCache.get(key);
        }
    }
}
