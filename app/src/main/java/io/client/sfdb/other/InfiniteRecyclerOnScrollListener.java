package io.client.sfdb.other;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import io.client.sfdb.BuildConfig;

/**
 * Allow to load films infinity amount.
 *
 * Created by simoala-kotila on 16/08/16.
 */
public class InfiniteRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    // The minimum number of items remaining before we should mLoading more.
    private static final int VISIBLE_THRESHOLD = 6;
    private static final String TAG = "InfiniteRecyclerOn";

    private LinearLayoutManager mLinearLayoutManager;
    private OnLoadMoreListener onLoadMoreListener;

    // The current offset index of data you have loaded
    private int mCurrentPage = 2;
    private int mPreviousTotal;
    private boolean mLoading;
    private boolean mIsMoreDataLoad = true;


    public InfiniteRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();


        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
                mCurrentPage++;
            }
        }
        if (!mLoading && mIsMoreDataLoad && (firstVisibleItem + visibleItemCount + VISIBLE_THRESHOLD) >= (totalItemCount)) {
            mLoading = true;
            onLoadMoreListener.onLoadMore(mCurrentPage);
        }
        if(BuildConfig.DEBUG){
//            Log.d(TAG, String.format("visibleItemCount=%d, firstVisibleItem=%d, totalItemCount=%d currentPage=%d previousTotal=%d isMoreDataLoad=%b loading=%b",
//                    visibleItemCount,
//                    firstVisibleItem,
//                    totalItemCount,
//                    mCurrentPage,
//                    mPreviousTotal,
//                    mIsMoreDataLoad,
//                    mLoading));
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener){
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setMoreData(boolean isMoreDataLoad) {
        mIsMoreDataLoad = isMoreDataLoad;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    public interface OnLoadMoreListener {

        void onLoadMore(int mCurrentPage);
    }
}
