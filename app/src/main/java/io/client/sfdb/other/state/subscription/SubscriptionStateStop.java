package io.client.sfdb.other.state.subscription;

import io.client.sfdb.BasePresenter;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 17:35
 */

public class SubscriptionStateStop implements SubscriptionState {

    @Override
    public void doSubscription(BasePresenter holder) {
        //do nothing
    }
}
