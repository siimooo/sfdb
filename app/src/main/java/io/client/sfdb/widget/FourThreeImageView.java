package io.client.sfdb.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

/**
 * A extension of ForegroundImageView that is always 4:3 aspect ratio.
 *
 * Created by simoala-kotila on 29/05/16.
 */
public class FourThreeImageView extends ForegroundImageView {

    public FourThreeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int fourThreeHeight = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec) * 3 / 4, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, fourThreeHeight);
    }

}