package io.client.sfdb;

/**
 * Created by simoala-kotila on 06/04/16.
 */
public interface BaseView<T> {

    void setPresenter(T presenter);
}
