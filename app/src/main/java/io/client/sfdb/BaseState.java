package io.client.sfdb;

/**
 * Taken from https://pspdfkit.com/blog/2016/restoring-state-in-android-mvp-architecture/
 *
 * User: Simo Ala-Kotila
 * Date: 27/12/16
 * Time: 09:25
 */

public class BaseState {

    interface BaseStatefulPresenter<V extends BaseView, S extends BaseState> {
        void subscribe(V view, S state);
        S getState();
    }
}
