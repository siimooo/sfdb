package io.client.sfdb.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.rxjavaomdb.network.moshi.OMDbTitleOrId;


/**
 * Gather bitmapkey and film to same class
 *
 * Created by simoala-kotila on 10/07/16.
 */
public class Film implements Parcelable {

    private final OMDbTitleOrId mFilm;

    private Film(Builder builder){
        mFilm = builder.mFilm;
    }

    public OMDbTitleOrId getFilm() {
        return mFilm;
    }

    @Override
    public String toString() {
        return "Film{" +
                "mFilm=" + mFilm +
                '}';
    }

    public static class Builder {

        private OMDbTitleOrId mFilm;

        public Builder setFilm(OMDbTitleOrId film){
            mFilm = film;
            return this;
        }

        public Film build(){
            return new Film(this);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.mFilm);
    }

    protected Film(Parcel in) {
        this.mFilm = (OMDbTitleOrId) in.readSerializable();
    }

    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel source) {
            return new Film(source);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };
}
