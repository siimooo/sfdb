package io.client.sfdb;

/**
 * Created by simoala-kotila on 06/04/16.
 */
public interface BasePresenter {

    void subscribe();

    void unsubscribe();
}
