package io.client.sfdb.feed;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import io.client.sfdb.Injection;
import io.client.sfdb.R;
import io.client.sfdb.util.ActivityUtils;

/**
 * Displays an FeedView "home screen" of the application.
 *
 * Created by simoala-kotila on 06/04/16.
 */
public class FeedActivity extends AppCompatActivity {

    private FeedPresenter mFeedPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_activity);

        //set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_drawable);//just temp icon
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        //set up fragment
        FeedFragment feedFragment = (FeedFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (feedFragment == null) {
            // Create the fragment
            feedFragment = feedFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), feedFragment, R.id.contentFrame);
        }

//        ContextCompat.getColor(FeedActivity.this, R.color.)
        // Create the presenter
        mFeedPresenter = new FeedPresenter(feedFragment,
                Injection.getInstance().provideOMDbClient(),
                Injection.getInstance().provideTheMovieDBClient(),
                Injection.getInstance().providerSchedulerProvider(),
                Injection.getInstance().providePicasso(getApplicationContext()));


        //load previous state if possible
        if(savedInstanceState != null){

        }

        //TODO:
        //1. State when screen rotates
        //------> index onLoadFilms
        //2. State when comes back from details activity

    }
}