package io.client.sfdb.feed;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import io.client.sfdb.BuildConfig;
import io.client.sfdb.model.Film;
import io.client.sfdb.util.ApplicationUtils;
import io.client.sfdb.other.schedulers.BaseSchedulerProvider;
import io.rxjavaokhttp.ClientIOException;
import io.rxjavaomdb.ApiException;
import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.rxjavaomdb.utils.Parser;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBUpcoming;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;

/**
 * Listens to user actions from the UI ({@link FeedFragment}),
 * retrieves the data and updates the UI as required.
 *
 * Created by simoala-kotila on 06/04/16
 */
public class FeedPresenter implements FeedContract.Presenter {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private static final String TAG = "FeedPresenter";

    private FeedContract.View mFeedView;

    private OMDbClient mOMDbClient;

    private BaseSchedulerProvider mSchedulerProvider;

    //to avoid damn memory leak
    private CompositeSubscription mSubscriptions;

    private TheMovieDBClient mTheMovieDBClient;

    private Picasso mPicasso;

    public FeedPresenter(FeedContract.View feedView, OMDbClient omdbClient, TheMovieDBClient theMovieDBClient, BaseSchedulerProvider schedulerProvider, Picasso picasso){
        mFeedView = feedView;
        mFeedView.setPresenter(this);
        mOMDbClient = omdbClient;
        mSchedulerProvider = schedulerProvider;
        mTheMovieDBClient = theMovieDBClient;
        mSubscriptions = new CompositeSubscription();
        mPicasso = picasso;
    }

    @Override
    public void searchFilm(String criteria) {
        //TODO: handle if film not found
        Subscription subscription = mOMDbClient.requestByTitle(criteria).subscribeOn(mSchedulerProvider.io()).observeOn(mSchedulerProvider.ui()).subscribe(new Subscriber<OMDbTitleOrId>() {
            @Override
            public void onCompleted() { }

            @Override
            public void onError(Throwable throwable) {
                Log.e(TAG, String.format("searchFilm OnError error=[%s]", throwable.getMessage()));
                throwable.printStackTrace();
                mFeedView.showSearchErrorResult();
            }

            @Override
            public void onNext(OMDbTitleOrId film) {
                mFeedView.showSearchResult(film);
            }
        });
        mSubscriptions.add(subscription);//add it to subscribe list
    }

    @Override
    public void loadFilms(final int index) {
        if(BuildConfig.DEBUG){
            Log.d(TAG, String.format("loadFilms index=[%d]", index));
        }
        Subscription subscription = mTheMovieDBClient
                .upcoming(index)
                .filter(new Func1<TheMovieDBUpcoming, Boolean>() {
                    @Override
                    public Boolean call(TheMovieDBUpcoming theMovieDBUpcoming) {
                        if(BuildConfig.DEBUG){
                            Log.d(TAG, String.format("TheMovieDBUpcoming totalPages=[%d]", theMovieDBUpcoming.getTotal_pages()));
                        }
                        if(theMovieDBUpcoming.getTotal_pages() < index){
                            if(BuildConfig.DEBUG){
                                Log.d(TAG, String.format("TheMovieDBUpcoming showMoreDataLoad=%b", false));
                            }
                            mFeedView.showMoreDataLoad(false);
                            return false;
                        }else{
                            Log.d(TAG, String.format("TheMovieDBUpcoming showMoreDataLoad=%b", true));
                            mFeedView.showMoreDataLoad(true);
                        }
                        return true;
                    }
                })
                .subscribeOn(mSchedulerProvider.io())
                .flatMap(new Func1<TheMovieDBUpcoming, Observable<TheMovieDBMovieUpcomingResults>>() {
                    @Override
                    public Observable<TheMovieDBMovieUpcomingResults> call(TheMovieDBUpcoming theMovieDBUpcoming) {
                        return Observable.from(theMovieDBUpcoming.getResults());
                    }
                }).flatMap(new Func1<TheMovieDBMovieUpcomingResults, Observable<OMDbTitleOrId>>() {
                    @Override
                    public Observable<OMDbTitleOrId> call(TheMovieDBMovieUpcomingResults theMovieDBMovieUpcomingResults) {
                        String releaseDate = theMovieDBMovieUpcomingResults.getReleaseDate();
                        String year = Integer.toString(Parser.parseYear(releaseDate, DATE_FORMAT));
                        String title = theMovieDBMovieUpcomingResults.getTitle();
                        return mOMDbClient.requestByTitle(title, true, year).subscribeOn(mSchedulerProvider.io()).onErrorResumeNext(new Func1<Throwable, Observable<? extends OMDbTitleOrId>>() {
                            @Override
                            public Observable<? extends OMDbTitleOrId> call(Throwable throwable) {
                                if(throwable instanceof ClientIOException){
                                    ClientIOException clientIOException = (ClientIOException) throwable;
                                    Log.e(TAG, String.format("onErrorResumeNext OMDbTitleOrId onError message[%s], errorCode=[%d], url=[%s]", clientIOException.getMessage(), clientIOException.getErrorCode(), clientIOException.getUrl()));
                                }else if (throwable instanceof ApiException){
                                    ApiException clientIOException = (ApiException) throwable;
                                    Log.e(TAG, String.format("onErrorResumeNext OMDbTitleOrId onError message[%s], errorCode=[%d]", clientIOException.getMessage(), clientIOException.getErrorCode()));
                                }else{
                                    Log.e(TAG, String.format("onErrorResumeNext OMDbTitleOrId unknown error onError message[%s] ", throwable.getMessage()));
                                    return Observable.error(throwable);
                                }
                                return Observable.empty();
                            }
                        });
                    }
                }).filter(new Func1<OMDbTitleOrId, Boolean>() {
                    @Override
                    public Boolean call(OMDbTitleOrId omDbTitleOrId) {
                        return !omDbTitleOrId.getPoster().equals(Parser.N_A_VALUE);
                    }
                }).flatMap(new Func1<OMDbTitleOrId, Observable<Film>>() {
                    @Override
                    public Observable<Film> call(final OMDbTitleOrId film) {
                        return Observable.just(new Film.Builder().setFilm(film).build());
                    }
                })//filter after observerOn here and do ui updates
                .observeOn(mSchedulerProvider.ui()).subscribe(new Subscriber<Film>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        if(e instanceof ClientIOException){
                            ClientIOException clientIOException = (ClientIOException) e;
                            Log.e(TAG, String.format("loadFilms onError message[%s], errorCode=[%d], url=[%s]", clientIOException.getMessage(), clientIOException.getErrorCode(), clientIOException.getUrl()));
                        }
                        e.printStackTrace();
                        mFeedView.showFilmsFeedError();
                    }

                    @Override
                    public void onNext(Film filmPoster) {
                        mFeedView.showLoadedFilmPoster(filmPoster);
                    }
                });
        mSubscriptions.add(subscription);//add it to subscribe list
    }

    @Override
    public void posterLongClick(OMDbTitleOrId omDbTitleOrId, TextView textView) {
        if(BuildConfig.DEBUG){
            Log.d(TAG, String.format("posterLongClick"));
        }
        mFeedView.showFilmPreview(omDbTitleOrId, textView);
    }

    @Override
    public void posterLongClickReleased(TextView textView) {
        if(BuildConfig.DEBUG){
            Log.d(TAG, String.format("posterLongClickReleased"));
        }
        mFeedView.hideFilmPreview(textView);
    }

    @Override
    public void retry(int index) {
        loadFilms(index);
    }

    @Override
    public void loadPoster(String url, ImageView imageView, int index) {
        RequestCreator creator = mPicasso.load(url);
        creator.centerCrop().resize(ApplicationUtils.POSTER_WIDTH, ApplicationUtils.POSTER_HEIGHT);
        mFeedView.showPoster(creator, imageView, index);
    }

    @Override
    public void openFilmDetail(OMDbTitleOrId omDbTitleOrId) {
        mFeedView.showFilmDetail(omDbTitleOrId);
    }

    @Override
    public void subscribe(int index) {
        mFeedView.setLoadingIndicator(true);
        loadFilms(index);
    }

    @Override
    public void subscribe() { }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }
}