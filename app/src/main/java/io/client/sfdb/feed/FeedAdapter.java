package io.client.sfdb.feed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.client.sfdb.R;
import io.client.sfdb.model.Film;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;

/**
 * Just bind data to it. Do NOT add anything else.
 * Adapter for displaying a grid of {@link io.rxjavaomdb.repository.FilmRepository}s and {@link io.rxjavaomdb.repository.PosterRepository}s.
 *
 * Created by simoala-kotila on 13/04/16.
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "FeedAdapter";

    /**
     * Feeds ViewState
     */
    public static final int FEED = 1;

    /**
     * Loading more ViewState
     */
    public static final int LOADING_MORE = 2;

    //Listener for the ListClicks
    private FeedItemListener mFeedItemListener;

    private boolean mIsInfiniteScroll = true;

    /**
     * Just the context (Activity)
     */
    private final Context mContext;

//    private FeedContract.Presenter presenter;

    public void changeInfiniteScrollItemView(View view, final boolean isInfiniteScroll){
        // Delay before notifying the adapter since the scroll listeners
        // can be called while RecyclerView data cannot be changed.

        view.post(new Runnable() {
            @Override
            public void run() {
                mIsInfiniteScroll = isInfiniteScroll;
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Hold data of the mFilm in this adapter
     */
    private List<Film> mFilm = new ArrayList<>();

    public FeedAdapter(Context context, FeedItemListener feedItemListener) {
        mContext = context;
        mFeedItemListener = feedItemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//Load layout to UI and setup Holder
        switch (viewType){
            case FEED:
                return setFeedLayout(parent);
            case LOADING_MORE:
                return setInfiniteLoadingLayout(parent);
            default:
                throw new IllegalArgumentException(String.format("unknown ViewType=[%d] onCreateViewHolder",viewType));
        }
    }

    protected LoadingMoreHolder setInfiniteLoadingLayout(ViewGroup parent){
        //inflate layout for RecyclerView
        View view = LayoutInflater.from(mContext).inflate(R.layout.feed_infinite_loading, parent, false);
        //create holder for layouts items.
        LoadingMoreHolder holder = new LoadingMoreHolder(view);
        return holder;
    }

    protected FilmPosterImageHolder setFeedLayout(ViewGroup parent){
        //inflate layout for RecyclerView
        View view = LayoutInflater.from(mContext).inflate(R.layout.feed_film_items, parent, false);
        //create holder for layouts items.
        FilmPosterImageHolder holder = new FilmPosterImageHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {//draw Holder to UI
        if(!mFilm.isEmpty() && position < mFilm.size()){
            bindFeedViewHolder(holder, position);
        }else{
            bindInfiniteViewHolder(holder, position);//for the infinite scroller
        }
    }

    protected void bindFeedViewHolder(final RecyclerView.ViewHolder holder, final int position){
        Film film = mFilm.get(position);
        final FilmPosterImageHolder imageHolder = (FilmPosterImageHolder) holder;
        mFeedItemListener.onLoadPoster(film.getFilm().getPoster(), imageHolder.mImage, position);

        imageHolder.mImage.setOnClickListener(new View.OnClickListener() {//normal click
            @Override
            public void onClick(View view) {
                mFeedItemListener.onPosterClick(mFilm.get(position).getFilm());
            }
        });
        imageHolder.mImage.setOnLongClickListener(new View.OnLongClickListener() {//long click
            @Override
            public boolean onLongClick(View view) {
                mFeedItemListener.onPosterLongClick(mFilm.get(position).getFilm(), imageHolder.mTextViewFilmScorePreview);
                return true;
            }
        });
        imageHolder.mImage.setOnTouchListener(new View.OnTouchListener() {//handle long click released
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP ||
                        motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    mFeedItemListener.onPosterLongClickReleased(imageHolder.mTextViewFilmScorePreview);
                }
                return false;
            }
        });
    }

    protected void bindInfiniteViewHolder(RecyclerView.ViewHolder holder, int position){
        /* Do not use this if using GridLayoutManager */
//        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
//        layoutParams.setFullSpan(true);
    }

    @Override
    public int getItemViewType(int position) {
        return position >= mFilm.size() ? LOADING_MORE : FEED;//add loading more if the position is higher than items count
    }

    @Override
    public int getItemCount() {
        if(mIsInfiniteScroll){
            return mFilm.size() + 1;//infinite scroll needs + 1 item on the this adapter
        }else{
            return mFilm.size();
        }

    }

    public void add(Film film){
        mFilm.add(film);
        notifyItemInserted(mFilm.size() - 1);//this will enable fadeIn animation for the RecyclerView. notifyDataSetChanged() will not have this feature
//        notifyDataSetChanged();
    }

    public void clear() {
        mFilm.clear();
        notifyDataSetChanged();
    }

    public List<Film> getFilms(){
        return mFilm;
    }

    public void addAll(List<Film> filmList) {
        mFilm.addAll(filmList);
        notifyDataSetChanged();
    }

    public static class FilmPosterImageHolder extends RecyclerView.ViewHolder {

        public View mView;
        public ImageView mImage;
        public TextView mTextViewFilmScorePreview;

        public FilmPosterImageHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mImage = (ImageView) itemView.findViewById(R.id.poster);
            mTextViewFilmScorePreview = (TextView) itemView.findViewById(R.id.film_score_preview);
        }
    }

    public static class LoadingMoreHolder extends RecyclerView.ViewHolder{

        private ProgressBar mProgressBar;

        public LoadingMoreHolder(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.infinite_loading);
        }
    }

    /**
     * Interface to handle user clicks
     */
    public interface FeedItemListener {

        void onLoadPoster(String url, ImageView imageView, int index);

        void onPosterClick(OMDbTitleOrId omDbTitleOrId);

        void onPosterLongClick(OMDbTitleOrId omDbTitleOrId, TextView textView);

        void onPosterLongClickReleased(TextView textView);
    }
}