package io.client.sfdb.feed;

import android.app.SearchManager;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.squareup.picasso.RequestCreator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.client.sfdb.BuildConfig;
import io.client.sfdb.R;
import io.client.sfdb.filmdetail.FilmDetailActivity;
import io.client.sfdb.model.Film;
import io.client.sfdb.other.InfiniteRecyclerOnScrollListener;
import io.client.sfdb.other.state.subscription.SubscriptionStateHolder;
import io.client.sfdb.other.state.subscription.SubscriptionStateHolder2;
import io.client.sfdb.other.state.subscription.SubscriptionStateLoad;
import io.client.sfdb.other.state.subscription.SubscriptionStateStop;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;

/**
 * Main UI for the film feed. User can scroll films, search them and touch them.
 *
 * Created by simoala-kotila on 06/04/16
 */
public class FeedFragment extends Fragment implements FeedContract.View,
                                                      SearchView.OnQueryTextListener,
                                                      InfiniteRecyclerOnScrollListener.OnLoadMoreListener{


    private static final String SAVED_CONTENT_VIEW = "content-view-state";
    private static final String SAVED_ADAPTER_ARRAY = "adapter-array-state";
    private static final String SAVED_LAYOUT_MANAGER = "layout-manager-state";
    private static final String SAVED_INFINITE_SCROLL = "infinite-scroll-index-state";

    private static final int NUM_OF_COLUMNS = 2;

    private static final String TAG = "FeedFragment";

    private FeedContract.Presenter mPresenter;

    private RecyclerView mRecyclerView;

    private ColorDrawable[] mPlaceholder;

    private GridLayoutManager mGridLayoutManager;

    private FeedAdapter mFeedAdapter;

    private MultiStateView mMultiStateView;

    private InfiniteRecyclerOnScrollListener mInfiniteRecyclerOnScrollListener;

    public static FeedFragment newInstance(){
        return new FeedFragment();
    }

    private SubscriptionStateHolder2 holder = new SubscriptionStateHolder2(mPresenter);

    public FeedFragment(){
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        holder.doSubscription();
//        mPresenter.subscribe();
        //Logic
        //load on start TRUE
        //load on on scroll TRUE
        //load on state change FALSE
        //load on resume activity FALSE
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.feed_fragment, container, false);
        setUpUIForFragment(root);
        setHasOptionsMenu(true);//enable overflow button (settings)
        return root;
    }

    @Override
    public void setPresenter(FeedContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_fragment_menu, menu);
        setupSearchView(menu, inflater);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showSearchResult(OMDbTitleOrId film) {
        Snackbar.make(getView(), film.toString(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showSearchNotFoundResult() {
        Snackbar.make(getView(), R.string.search_empty_film_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showSearchErrorResult() {
        Snackbar.make(getView(), R.string.search_error_film_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showFilmsFeedError() {
        mFeedAdapter.clear();
        mMultiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
        Button button = (Button) getView().findViewById(R.id.feed_retry);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.retry(1);//TODO: check this
                mRecyclerView.scrollToPosition(0);
            }
        });
    }

    @Override
    public void showLoadedFilmPoster(Film film) {
        mFeedAdapter.add(film);
        setLoadingIndicator(false);
    }

    @Override
    public void showFilmPreview(OMDbTitleOrId omDbTitleOrId, TextView mTextViewFilmScorePreview) {
        boolean isHidden = mTextViewFilmScorePreview.getVisibility() == View.GONE;
        if(isHidden){
            mTextViewFilmScorePreview.setVisibility(View.VISIBLE);
            mTextViewFilmScorePreview.setText(omDbTitleOrId.getTitle());
        }
    }

    @Override
    public void hideFilmPreview(TextView mTextViewFilmScorePreview) {
        boolean isVisible = mTextViewFilmScorePreview.getVisibility() == View.VISIBLE;
        if(isVisible){
            mTextViewFilmScorePreview.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMoreDataLoad(boolean isMoreDataLoad) {
        mInfiniteRecyclerOnScrollListener.setMoreData(isMoreDataLoad);
        mFeedAdapter.changeInfiniteScrollItemView(getView(), isMoreDataLoad);
        if(BuildConfig.DEBUG){
            Log.d(TAG, String.format("showMoreDataLoad adapterSize=%d",mFeedAdapter.getFilms().size()));
        }
    }

    @Override
    public void showPoster(RequestCreator creator, ImageView imageView, int index) {
        creator.placeholder(mPlaceholder[index % mPlaceholder.length]).into(imageView);
    }

    @Override
    public void showFilmDetail(OMDbTitleOrId omDbTitleOrId) {
        Intent intent = new Intent(getContext(), FilmDetailActivity.class);
        intent.putExtra(FilmDetailActivity.FILM_OBJECT, omDbTitleOrId);
        startActivity(intent);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if(active){
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }else{
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }

    protected void setupSearchView(Menu menu, MenuInflater inflater){
        // Retrieve the SearchView and plug it into SearchManager
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(this);
    }

    protected void setUpUIForFragment(View view){
        setPlaceholder();
        setMultiStateView(view);
        setFeedAdapter();
        setRecyclerView(view);
        setInfiniteScroll();
    }

    private void setPlaceholder() {
        TypedArray a = getContext().obtainStyledAttributes(R.styleable.DribbbleFeed);
        int loadingColorArrayId = a.getResourceId(R.styleable.DribbbleFeed_shotLoadingPlaceholderColors, 0);

        int[] placeholderColors = getContext().getResources().getIntArray(loadingColorArrayId);
        mPlaceholder = new ColorDrawable[placeholderColors.length];
        for (int i = 0; i < placeholderColors.length; i++) {
            mPlaceholder[i] = new ColorDrawable(placeholderColors[i]);
        }
    }

    protected void setMultiStateView(View view){
        mMultiStateView = (MultiStateView) view.findViewById(R.id.multiStateView);
    }

    protected void setFeedAdapter(){
        mFeedAdapter = new FeedAdapter(getContext(), mFeedItemListener);
    }

    protected void setRecyclerView(View view){
        //LayoutManager
//        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        mGridLayoutManager = new GridLayoutManager(getContext(), NUM_OF_COLUMNS);
        //RecyclerView
        mRecyclerView = (RecyclerView) view.findViewById(R.id.feed_grid);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mRecyclerView.setAdapter(mFeedAdapter);

        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {//change columns for the gridlayout depend of the adapter item state
            @Override
            public int getSpanSize(int position) {
                //TODO: possibility add feature where user has possibility to change column size
                //TODO: Also handle tablet screen
                if (mFeedAdapter.getItemViewType(position) == FeedAdapter.FEED) {
                    return 1;//default
                }
                return 2;//change two column to 1
            }
        });
    }

    protected void setInfiniteScroll(){
        mInfiniteRecyclerOnScrollListener = new InfiniteRecyclerOnScrollListener(mGridLayoutManager);
        mInfiniteRecyclerOnScrollListener.setOnLoadMoreListener(this);///set listener for load more
        mRecyclerView.addOnScrollListener(mInfiniteRecyclerOnScrollListener);//setup custom scroll listener
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //TODO: this should be easily handled by RxJava
        mPresenter.searchFilm(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //TODO: this should be easily handled by RxJava
        return true;
    }

    @Override
    public void onLoadMore(int mCurrentPage) {
        mPresenter.loadFilms(mCurrentPage);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save state
        saveState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        //load state
        loadState(savedInstanceState);
    }

    protected void loadState(Bundle savedInstanceState){
        // Load previously saved state, if available.
        if(savedInstanceState == null){
            return;
        }
        Parcelable state = savedInstanceState.getParcelable(SAVED_LAYOUT_MANAGER);
        mRecyclerView.getLayoutManager().onRestoreInstanceState(state);
        int viewState = savedInstanceState.getInt(SAVED_CONTENT_VIEW);
        mMultiStateView.setViewState(viewState);


        List<Film> filmBitmapKeyList = savedInstanceState.getParcelableArrayList(SAVED_ADAPTER_ARRAY);
        mFeedAdapter.addAll(filmBitmapKeyList);
        int index = savedInstanceState.getInt(SAVED_INFINITE_SCROLL);
        mInfiniteRecyclerOnScrollListener.setCurrentPage(index);
    }

    protected void saveState(Bundle bundle){
        bundle.putParcelable(SAVED_LAYOUT_MANAGER, mRecyclerView.getLayoutManager().onSaveInstanceState());
        bundle.putInt(SAVED_CONTENT_VIEW, mMultiStateView.getViewState());
        bundle.putParcelableArrayList(SAVED_ADAPTER_ARRAY, (ArrayList<? extends Parcelable>) mFeedAdapter.getFilms());
        bundle.putInt(SAVED_INFINITE_SCROLL, mInfiniteRecyclerOnScrollListener.getCurrentPage());
    }

    /**
     * Listener for clicks on tasks in the RecyclerView. Will contain in the ({@link FeedAdapter}) .
     *
     * Delegate it from ({@link FeedAdapter}) to ({@link FeedPresenter}), ({@link FeedPresenter}) to ({@link FeedFragment})
     */
    public FeedAdapter.FeedItemListener mFeedItemListener = new FeedAdapter.FeedItemListener() {

        @Override
        public void onLoadPoster(String url, ImageView image, int index) {
            mPresenter.loadPoster(url, image, index);
        }

        @Override
        public void onPosterClick(OMDbTitleOrId omDbTitleOrId) {
            mPresenter.openFilmDetail(omDbTitleOrId);
        }

        @Override
        public void onPosterLongClick(OMDbTitleOrId omDbTitleOrId, TextView textView) {
            mPresenter.posterLongClick(omDbTitleOrId, textView);
        }

        @Override
        public void onPosterLongClickReleased(TextView textView) {
            mPresenter.posterLongClickReleased(textView);
        }
    };
}