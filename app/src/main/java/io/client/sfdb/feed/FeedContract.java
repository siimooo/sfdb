package io.client.sfdb.feed;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.RequestCreator;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.BaseView;
import io.client.sfdb.model.Film;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;


/**
 * This specifies the contract between the view ({@link FeedFragment}) and the presenter ({@link FeedPresenter}).
 *
 * Created by simoala-kotila on 06/04/16
 */
public class FeedContract {

    /**
     *  Main UI for the ({@link FeedActivity})
     */
    interface View extends BaseView<Presenter> {

        boolean isActive();

        /**
         * Show found film from search
         *
         * @param film
         */
        void showSearchResult(OMDbTitleOrId film);

        /**
         * Show not found result if film didn't found from API
         */
        void showSearchNotFoundResult();

        /**
         * Show error if occurred on the film search
         */
        void showSearchErrorResult();

        /**
         * Show error if occurred on the films feed
         */
        void showFilmsFeedError();

        /**
         * Show loaded film and poster
         *
         * @param film
         */
        void showLoadedFilmPoster(Film film);

        /**
         * Show nice preview when has pressed long click on the poster
         *
         * @param omDbTitleOrId
         * @param textView
         */
        void showFilmPreview(OMDbTitleOrId omDbTitleOrId, TextView textView);

        /**
         * Hide preview when user has released or canceled long press
         *
         * @param textView
         */
        void hideFilmPreview(TextView textView);


        void showMoreDataLoad(boolean isMoreDataLoad);


        void showPoster(RequestCreator creator, ImageView imageView, int index);


        void showFilmDetail(OMDbTitleOrId omDbTitleOrId);

        void setLoadingIndicator(boolean active);

        //TODO: handle user query listener by RxJava
    }

    /**
     *  Listens to user actions from the UI, updates the UI as required.
     */
    public interface Presenter extends BasePresenter {

        /**
         * Search films by criteria
         *
         * @param criteria user written criteria. current support only movie title
         */
        void searchFilm(String criteria);

        /**
         * Load films by movieList
         *
         * @param index index of the page
         */
        void loadFilms(int index);

        /**
         * Handle long click from the listview (adapter)
         *
         * @param omDbTitleOrId
         */
        void posterLongClick(OMDbTitleOrId omDbTitleOrId, TextView textView);

        /**
         * Handle long click releasing from the listview (adapter)
         *
         */
        void posterLongClickReleased(TextView textView);

        /**
         * Listen onRetry click when error occurred during feed loading
         *
         * @param index index of the page
         */
        void retry(int index);

        /**
         * Load poster for the item.
         *
         * @param url
         * @param image
         */
        void loadPoster(String url, ImageView image, int index);

        /**
         * Handle click from the listview (adapter)
         *
         * @param omDbTitleOrId
         */
        void openFilmDetail(OMDbTitleOrId omDbTitleOrId);

        void subscribe(int index);
    }
}
