package io.client.sfdb.feed;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import io.client.sfdb.model.Film;
import io.client.sfdb.other.schedulers.BaseSchedulerProvider;
import io.client.sfdb.other.schedulers.ImmediateSchedulerProvider;
import io.client.sfdb.util.ApplicationUtils;
import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import io.rxjavathemoviedb.network.moshi.TheMovieDBDates;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBMovieUpcomingResults;
import io.rxjavathemoviedb.network.moshi.movie.TheMovieDBUpcoming;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;
import rx.Observable;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Create and run test for the ({@link FeedPresenter}).
 *
 * Created by simoala-kotila on 07/04/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class FeedPresenterTest {

    @Mock
    private OMDbClient mOMDbClient;

    @Mock
    private FeedContract.View mFeedView;

    @Mock
    private TheMovieDBClient theMovieDBClient;

    private FeedPresenter mFeedPresenter;

    private BaseSchedulerProvider schedulerProvider;

    @Mock
    private Film filmMock;

    @Mock
    private ImageView imageView;

    @Mock
    private TextView textViewMock;

    @Mock
    private Picasso picasso;

    @Mock
    private RequestCreator creator;

    @Before
    public void setupMocksAndView() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);


        schedulerProvider = new ImmediateSchedulerProvider();
        mFeedPresenter = new FeedPresenter(mFeedView, mOMDbClient, theMovieDBClient, schedulerProvider, picasso);

        // The presenter wont't update the view unless it's active.
        when(mFeedView.isActive()).thenReturn(true);
    }

    @Test
    public void feedRepository_searchFilm_showSuccessUI(){
        // When the presenter is asked to do search
        // mock
        OMDbTitleOrId titleOrId = new OMDbTitleOrId("batman v superman","1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, null,null, null, null, null, null, null, null);


        when(mOMDbClient.requestByTitle("batman v superman")).thenReturn(Observable.just(titleOrId));

        mFeedPresenter.searchFilm("batman v superman");

        ArgumentCaptor<OMDbTitleOrId> showFilmCapture = ArgumentCaptor.forClass(OMDbTitleOrId.class);
        // shown in the UI
        verify(mFeedView).showSearchResult(showFilmCapture.capture());
        assertTrue(showFilmCapture.getValue().getTitle() != null);
    }

    @Test
    public void feedRepository_loadFilms_showSuccessFeed(){
        //mock feed


        String title = "batman v superman";
        String title2 = "breaking bad";

        List<TheMovieDBMovieUpcomingResults> resultses = new ArrayList<>();
        List<Integer> genresId = new ArrayList<>();
        TheMovieDBMovieUpcomingResults results = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title, false,"2016-12-12", title);


        TheMovieDBMovieUpcomingResults results2 = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title2, false,"2016-12-11", title2);
        resultses.add(results);
        resultses.add(results2);
        TheMovieDBUpcoming upcoming = new TheMovieDBUpcoming(1, 10, 200,new TheMovieDBDates("1", "2"), resultses);


        OMDbTitleOrId titleOrId = new OMDbTitleOrId("batman v superman","1999", "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "1", null, null, null);
        OMDbTitleOrId titleOrId2 = new OMDbTitleOrId("breaking bad","1999", "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "2", null, null, null);

        when(theMovieDBClient.upcoming(1)).thenReturn(Observable.just(upcoming));
        when(mOMDbClient.requestByTitle("batman v superman",true, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle("batman v superman",true, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle("breaking bad",true,"2016")).thenReturn(Observable.just(titleOrId2));

        mFeedPresenter.loadFilms(1);
        ArgumentCaptor<Film> showFilmCapture = ArgumentCaptor.forClass(Film.class);

        when(picasso.load(Matchers.anyString())).thenReturn(creator);
        when(creator.centerCrop()).thenReturn(creator);
        when(creator.resize(ApplicationUtils.POSTER_WIDTH, ApplicationUtils.POSTER_HEIGHT)).thenReturn(creator);

        mFeedPresenter.loadPoster("https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg", imageView, 0);
        mFeedPresenter.loadPoster("https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg", imageView, 1);

        // shown in the UI
        verify(mFeedView, atLeast(2)).showLoadedFilmPoster(showFilmCapture.capture());
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 0);
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 1);

        //verify
        assertTrue(showFilmCapture.getValue().getFilm() != null);
    }



    @Test
    public void feedRepository_loadFilms_skipNAPosterPath(){
        //mock feed


        String title = "batman v superman";
        String title2 = "breaking bad";

        List<TheMovieDBMovieUpcomingResults> resultses = new ArrayList<>();
        List<Integer> genresId = new ArrayList<>();
        TheMovieDBMovieUpcomingResults results = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title, false,"2016-12-12", title);


        TheMovieDBMovieUpcomingResults results2 = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title2, false,"2016-12-11", title2);
        resultses.add(results);
        resultses.add(results2);
        TheMovieDBUpcoming upcoming = new TheMovieDBUpcoming(1, 10, 200,new TheMovieDBDates("1", "2"), resultses);

        when(theMovieDBClient.upcoming(1)).thenReturn(Observable.just(upcoming));

        OMDbTitleOrId titleOrId = new OMDbTitleOrId("batman v superman","1999", "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "N/A",null, null, null, "1", null, null, null);
        OMDbTitleOrId titleOrId2 = new OMDbTitleOrId("breaking bad","1999", "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "2", null, null, null);

        when(mOMDbClient.requestByTitle("batman v superman",true, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle("batman v superman",true, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle("breaking bad",true,"2016")).thenReturn(Observable.just(titleOrId2));

        mFeedPresenter.loadFilms(1);
        ArgumentCaptor<Film> showFilmCapture = ArgumentCaptor.forClass(Film.class);

        // shown in the UI
        verify(mFeedView, atLeast(1)).showLoadedFilmPoster(showFilmCapture.capture());

        //verify
        assertTrue(showFilmCapture.getValue().getFilm() != null);
    }



    @Test
    public void feedRepository_retry(){


        String title = "batman v superman";
        String title2 = "breaking bad";

        List<TheMovieDBMovieUpcomingResults> resultses = new ArrayList<>();
        List<Integer> genresId = new ArrayList<>();
        TheMovieDBMovieUpcomingResults results = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title, false,"2016-12-12", title);


        TheMovieDBMovieUpcomingResults results2 = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title2, false,"2016-12-11", title2);
        resultses.add(results);
        resultses.add(results2);
        TheMovieDBUpcoming upcoming = new TheMovieDBUpcoming(1, 10, 200,new TheMovieDBDates("1", "2"), resultses);

        when(theMovieDBClient.upcoming(1)).thenReturn(Observable.just(upcoming));
        OMDbTitleOrId titleOrId = new OMDbTitleOrId(title,"1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "1", null, null, null);

        OMDbTitleOrId titleOrId2 = new OMDbTitleOrId(title2,"1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "2", null, null, null);
        when(mOMDbClient.requestByTitle(title, true, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle(title2, true, "2016")).thenReturn(Observable.just(titleOrId2));

        //mock feed
        mFeedPresenter.retry(1);//will call load films again
        // shown in the UI
        ArgumentCaptor<Film> showFilmCapture = ArgumentCaptor.forClass(Film.class);
        // shown in the UI
        verify(mFeedView, atLeast(2)).showLoadedFilmPoster(showFilmCapture.capture());
    }


    @Test
    public void feedRepository_error_poster_during_feed_loading(){
        //mock feed
        String title = "batman v superman";
        String title2 = "breaking bad";

        List<TheMovieDBMovieUpcomingResults> resultses = new ArrayList<>();
        List<Integer> genresId = new ArrayList<>();
        TheMovieDBMovieUpcomingResults results = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title, false,"2016-12-12", title);


        TheMovieDBMovieUpcomingResults results2 = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title2, false,"2016-12-11", title2);
        resultses.add(results);
        resultses.add(results2);
        TheMovieDBUpcoming upcoming = new TheMovieDBUpcoming(1, 10, 200,new TheMovieDBDates("1", "2"), resultses);

        when(theMovieDBClient.upcoming(1)).thenReturn(Observable.just(upcoming));

        OMDbTitleOrId titleOrId = new OMDbTitleOrId(title,"1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "1", null, null, null);

        OMDbTitleOrId titleOrId2 = new OMDbTitleOrId(title2,"1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "2", null, null, null);

        when(mOMDbClient.requestByTitle(title,false, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle(title2,false, "2016")).thenReturn(Observable.just(titleOrId2));

        when(picasso.load(Matchers.anyString())).thenReturn(creator);
        when(creator.centerCrop()).thenReturn(creator);
        when(creator.resize(ApplicationUtils.POSTER_WIDTH, ApplicationUtils.POSTER_HEIGHT)).thenReturn(creator);

        mFeedPresenter.loadFilms(1);
        mFeedPresenter.loadPoster("https://images-na.ssl-jpg", imageView, 0);
        mFeedPresenter.loadPoster("https://images-na.ssl-imaZTgwMDk3OTcyMDE@._V1_SX300.jpg", imageView, 1);
        ArgumentCaptor<Film> showFilmCapture = ArgumentCaptor.forClass(Film.class);
        verify(mFeedView, atMost(2)).showLoadedFilmPoster(showFilmCapture.capture());
        // verify shown in the UI
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 0);
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 1);

    }


    @Test
    public void feedRepository_show_poster(){
        when(picasso.load("https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg")).thenReturn(creator);
        when(creator.centerCrop()).thenReturn(creator);
        when(creator.resize(ApplicationUtils.POSTER_WIDTH, ApplicationUtils.POSTER_HEIGHT)).thenReturn(creator);

        mFeedPresenter.loadPoster("https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg", imageView, 0);
        mFeedPresenter.loadPoster("https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg", imageView, 1);

        // verify shown in the UI
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 0);
        verify(mFeedView, atLeast(1)).showPoster(creator, imageView, 1);
    }

    @Test
    public void feedRepository_error_film_during_feed_loading(){
        //mock feed


        String title = "batman v superman";
        String title2 = "breaking bad";

        List<TheMovieDBMovieUpcomingResults> resultses = new ArrayList<>();
        List<Integer> genresId = new ArrayList<>();
        TheMovieDBMovieUpcomingResults results = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title, false,"2016-12-12", title);


        TheMovieDBMovieUpcomingResults results2 = new TheMovieDBMovieUpcomingResults("",
                false,
                "",
                20.00,
                1,
                1111,
                "en",
                genresId, title2, false,"2016-12-11", title2);
        resultses.add(results);
        resultses.add(results2);
        TheMovieDBUpcoming upcoming = new TheMovieDBUpcoming(1, 10, 200,new TheMovieDBDates("1", "2"), resultses);

        when(theMovieDBClient.upcoming(1)).thenReturn(Observable.just(upcoming));

        OMDbTitleOrId titleOrId = new OMDbTitleOrId(title,"1999", "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, "https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ0ODYzODc0OV5BMl5BanBnXkFtZTgwMDk3OTcyMDE@._V1_SX300.jpg",null, null, null, "1", null, null, null);
        when(mOMDbClient.requestByTitle(title, false, "2016")).thenReturn(Observable.just(titleOrId));
        when(mOMDbClient.requestByTitle(title2, false, "2016")).thenThrow(new RuntimeException());

        mFeedPresenter.loadFilms(1);

        // verify error screen shown in the UI
        ArgumentCaptor<Film> showFilmCapture = ArgumentCaptor.forClass(Film.class);
        verify(mFeedView, atMost(1)).showLoadedFilmPoster(showFilmCapture.capture());
        // verify error screen shown in the UI
        verify(mFeedView).showFilmsFeedError();
    }


    @Test
    public void feedRepository_posterLongClick_showFilmPreview(){
        //mock feed
        OMDbTitleOrId titleOrId = new OMDbTitleOrId("asdas","1999",
                "B", "True", null, "movie", "22","movie",null, null, null,null, null, null, null,null, null, null, "1", null, null, null);

        mFeedPresenter.posterLongClick(titleOrId, textViewMock);
        // shown in the UI
        verify(mFeedView).showFilmPreview(titleOrId, textViewMock);
    }

    @Test
    public void feedRepository_posterLongClickReleased_hideFilmPreview(){
        //mock feed
        mFeedPresenter.posterLongClickReleased(textViewMock);
        // shown in the UI
        verify(mFeedView).hideFilmPreview(textViewMock);
    }

}