package io.client.sfdb.other.state;

import org.junit.Test;

import io.client.sfdb.BasePresenter;
import io.client.sfdb.other.state.subscription.SubscriptionStateHolder;
import io.client.sfdb.other.state.subscription.SubscriptionStateLoad;
import io.client.sfdb.other.state.subscription.SubscriptionStateStop;

/**
 * User: Simo Ala-Kotila
 * Date: 26/12/16
 * Time: 17:36
 */

public class SubscriptionStateTest {


    @Test
    public void test(){
        BasePresenter presenter = new BasePresenter() {
            @Override
            public void subscribe() {
                System.out.println("subscribe");
            }

            @Override
            public void unsubscribe() {
                System.out.println("unsubscribe");
            }
        };
        SubscriptionStateHolder subscriptionStateHolder = new SubscriptionStateHolder(presenter);
        subscriptionStateHolder.setState(new SubscriptionStateLoad());
        subscriptionStateHolder.doSubscription();

        subscriptionStateHolder.setState(new SubscriptionStateStop());
        subscriptionStateHolder.doSubscription();







    }

}
