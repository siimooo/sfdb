package io.client.sfdb.other;

import android.graphics.Bitmap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * User: Simo Ala-Kotila
 * Date: 20/10/16
 * Time: 13:13
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class LRUCacheTest {

    private int w = 100;
    private int h = 100;

    @Test
    public void testAddBitmapToMemoryCache(){
        LRUCache.getInstance().clear();
        Bitmap bmp = createEmtyBitmap(25,30);
        LRUCache.getInstance().addBitmapToMemoryCache("1", bmp);
        Bitmap bitmap = LRUCache.getInstance().getBitmapFromMemCache("1");
        assertNotNull(bitmap);
        assertEquals(bitmap.getHeight(), 30);
        assertEquals(bitmap.getWidth(), 25);
    }

    @Test
    public void testIsBitmapInMemory(){
        LRUCache.getInstance().clear();
        Bitmap bmp = createEmtyBitmap(25,30);
        LRUCache.getInstance().addBitmapToMemoryCache("1", bmp);
        boolean isInMemory = LRUCache.getInstance().isBitmapInMemory("1");
        assertEquals(isInMemory, true);
        boolean isInMemory2 = LRUCache.getInstance().isBitmapInMemory("2");
        assertEquals(isInMemory2, false);
    }

    @Test
    public void testGetBitmapFromMemCache(){
        LRUCache.getInstance().clear();
        Bitmap bmp = createEmtyBitmap(25,30);
        LRUCache.getInstance().addBitmapToMemoryCache("1", bmp);
        Bitmap bitmap = LRUCache.getInstance().getBitmapFromMemCache("1");
        assertNotNull(bitmap);
        assertEquals(bitmap.getHeight(), 30);
        assertEquals(bitmap.getWidth(), 25);
    }

    private Bitmap createEmtyBitmap(int width, int height){
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(width, height, conf);
        return bmp;
    }
}