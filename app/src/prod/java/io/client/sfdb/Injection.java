/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.client.sfdb;

import android.content.Context;

import com.squareup.picasso.Picasso;

import io.client.sfdb.other.schedulers.BaseSchedulerProvider;
import io.client.sfdb.other.schedulers.SchedulerProvider;
import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.OMDbClientImpl;
import io.rxjavaomdb.network.PosterClient;
import io.rxjavaomdb.network.PosterClientImpl;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;
import io.rxjavathemoviedb.network.movies.TheMovieDBClientImpl;

/**
 * Enables injection of production implementations for at compile time.
 */
public class Injection {


    private final OMDbClient mOMDbClient;
    private final TheMovieDBClient theMovieDBClient;

    private static class SingletonHolder {
        public final static Injection INSTANCE = new Injection();
    }

    public static Injection getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private Injection(){
        theMovieDBClient = new TheMovieDBClientImpl();
        mOMDbClient = new OMDbClientImpl();
    }

    public TheMovieDBClient provideTheMovieDBClient(){
        return theMovieDBClient;
    }

    public OMDbClient provideOMDbClient() {
        return mOMDbClient;
    }

    public Picasso providePicasso(Context context){
        return Picasso.with(context);
    }

    public BaseSchedulerProvider providerSchedulerProvider(){
        return SchedulerProvider.getInstance();
    }
}
