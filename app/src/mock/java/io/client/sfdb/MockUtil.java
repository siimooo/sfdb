package io.client.sfdb;

import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by simoala-kotila on 21/08/16.
 */
public final class MockUtil {

    private static final String POSTER_PATH_BASE = "images/M/%s._V1_SX640.jpg]";

    private MockUtil(){}

    public static String createPosterPath(String imdbId){
        return String.format(POSTER_PATH_BASE, imdbId);
    }

    public static byte[] ISToByteArray(InputStream input) {
        try {
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while ((bytesRead = input.read(buffer)) != -1){
                output.write(buffer, 0, bytesRead);
            }
            return output.toByteArray();
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public static InputStream getMockPoster(String fileName, Context context){
        try {
            return context.getAssets().open(fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
