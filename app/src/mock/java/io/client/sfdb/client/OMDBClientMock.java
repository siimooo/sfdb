package io.client.sfdb.client;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.rxjavaomdb.ApiException;
import io.rxjavaomdb.network.OMDbClient;
import io.rxjavaomdb.network.moshi.OMDbTitleOrId;
import rx.Observable;
import rx.functions.Func1;

/**
 * User: Simo Ala-Kotila
 * Date: 27/11/16
 * Time: 17:05
 */

public class OMDBClientMock implements OMDbClient {

    private static final Map<String, OMDbTitleOrId> OMDB_DATA = new LinkedHashMap<>();

    public OMDBClientMock(){
        try {
            String json1 = "{\"Title\":\"Hacksaw Ridge\",\"Year\":\"2016\",\"Rated\":\"R\",\"Released\":\"04 Nov 2016\",\"Runtime\":\"139 min\",\"Genre\":\"Drama, History, War\",\"Director\":\"Mel Gibson\",\"Writer\":\"Robert Schenkkan (screenplay), Andrew Knight (screenplay)\",\"Actors\":\"Andrew Garfield, Richard Pyros, Jacob Warner, Milo Gibson\",\"Plot\":\"WWII American Army Medic Desmond T. Doss, who served during the Battle of Okinawa, refuses to kill people, and becomes the first man in American history to win the Medal of Honor without firing a shot.\",\"Language\":\"English, Japanese\",\"Country\":\"Australia, USA\",\"Awards\":\"3 wins & 11 nominations.\",\"Poster\":\"https://images-na.ssl-images-amazon.com/images/M/MV5BMjQ1NjM3MTUxNV5BMl5BanBnXkFtZTgwMDc5MTY5OTE@._V1_SX300.jpg\",\"Metascore\":\"71\",\"imdbRating\":\"8.6\",\"imdbVotes\":\"24,307\",\"imdbID\":\"tt2119532\",\"Type\":\"movie\",\"Response\":\"True\"}";
            String json2 = "{\"Title\":\"The Infiltrator\",\"Year\":\"2016\",\"Rated\":\"R\",\"Released\":\"13 Jul 2016\",\"Runtime\":\"127 min\",\"Genre\":\"Biography, Crime, Drama\",\"Director\":\"Brad Furman\",\"Writer\":\"Ellen Sue Brown (screenplay), Robert Mazur (based on the book on)\",\"Actors\":\"Bryan Cranston, Leanne Best, Daniel Mays, Tom Vaughan-Lawlor\",\"Plot\":\"A U.S. Customs official uncovers a money laundering scheme involving Colombian drug lord Pablo Escobar.\",\"Language\":\"English, Spanish\",\"Country\":\"UK\",\"Awards\":\"N/A\",\"Poster\":\"https://images-na.ssl-images-amazon.com/images/M/MV5BMTEwNzM2NjY2MTNeQTJeQWpwZ15BbWU4MDQ3MDI3Njgx._V1_SX300.jpg\",\"Metascore\":\"66\",\"imdbRating\":\"7.1\",\"imdbVotes\":\"22,353\",\"imdbID\":\"tt1355631\",\"Type\":\"movie\",\"Response\":\"True\"}";

            String moanaJSON = "{\"Title\":\"Moana\",\"Year\":\"2016\",\"Rated\":\"PG\",\"Released\":\"23 Nov 2016\",\"Runtime\":\"107 min\",\"Genre\":\"Animation, Adventure, Comedy\",\"Director\":\"Ron Clements, Don Hall, John Musker, Chris Williams\",\"Writer\":\"Jared Bush (screenplay), Ron Clements (story by), John Musker (story by), Chris Williams (story by), Don Hall (story by), Pamela Ribon (story by), Aaron Kandell (story by), Jordan Kandell (story by)\",\"Actors\":\"Auli'i Cravalho, Dwayne Johnson, Rachel House, Temuera Morrison\",\"Plot\":\"In Ancient Polynesia, when a terrible curse incurred by Maui reaches an impetuous Chieftain's daughter's island, she answers the Ocean's call to seek out the demigod to set things right.\",\"Language\":\"English\",\"Country\":\"USA\",\"Awards\":\"Nominated for 2 Golden Globes. Another 4 wins & 36 nominations.\",\"Poster\":\"https://images-na.ssl-images-amazon.com/images/M/MV5BMjI4MzU5NTExNF5BMl5BanBnXkFtZTgwNzY1MTEwMDI@._V1_SX300.jpg\",\"Metascore\":\"81\",\"imdbRating\":\"8.1\",\"imdbVotes\":\"22,873\",\"imdbID\":\"tt3521164\",\"Type\":\"movie\",\"Response\":\"True\"}";

            Moshi moshi = new Moshi.Builder().build();
            JsonAdapter<OMDbTitleOrId> jsonAdapter = moshi.adapter(OMDbTitleOrId.class);

            OMDbTitleOrId omDbTitleOrId = jsonAdapter.fromJson(json1);
            OMDbTitleOrId omDbTitleOrId2 = jsonAdapter.fromJson(json2);
            OMDbTitleOrId moana = jsonAdapter.fromJson(moanaJSON);

            addOMDb(omDbTitleOrId, omDbTitleOrId2, moana);
        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Observable<OMDbTitleOrId> requestByTitle(String s) {
        return requestByTitle(s, false, "short");
    }

    @Override
    public Observable<OMDbTitleOrId> requestByTitle(String s, boolean b) {
        return requestByTitle(s, b, "short");
    }

    @Override
    public Observable<OMDbTitleOrId> requestByTitle(String s, boolean b, String s1) {
        return Observable.just(s).flatMap(new Func1<String, Observable<OMDbTitleOrId>>() {
            @Override
            public Observable<OMDbTitleOrId> call(String s) {
                if(OMDB_DATA.get(s) == null){
                    try{
                        throw new ApiException("Movie not found!", ApiException.NOT_FOUND_MOVIE_CODE);
                    }catch (IOException e){
                        return Observable.error(e);
                    }
                }
                return Observable.just(OMDB_DATA.get(s));
            }
        }).delay(4, TimeUnit.SECONDS);
    }

    @Override
    public Observable<OMDbTitleOrId> requestByIMDbId(String s) {
        return Observable.empty();
    }

    public void addOMDb(OMDbTitleOrId... omDbTitleOrIds) {
        for (OMDbTitleOrId omDbTitleOrId : omDbTitleOrIds) {
            OMDB_DATA.put(omDbTitleOrId.getTitle(), omDbTitleOrId);
        }
    }

    public OMDbTitleOrId error(String response, String error){
        return new OMDbTitleOrId(null, null, null, response, error, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
}