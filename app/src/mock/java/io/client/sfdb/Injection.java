/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.client.sfdb;


import android.content.Context;

import com.squareup.picasso.Picasso;

import io.client.sfdb.client.OMDBClientMock;
import io.client.sfdb.client.TheMovieDbMock;
import io.client.sfdb.other.schedulers.BaseSchedulerProvider;
import io.client.sfdb.other.schedulers.SchedulerProvider;
import io.rxjavaomdb.network.OMDbClient;
import io.rxjavathemoviedb.network.movies.TheMovieDBClient;

/**
 * Enables injection of mock implementations for
 * {@link } at compile time. This is useful for testing, since it allows us to use
 * a fake instance of the class to isolate the dependencies and run a test hermetically.
 */
public class Injection {

    private final OMDbClient mOMDbClient;
    private final TheMovieDBClient mTheMovieDBClient;


    public TheMovieDBClient provideTheMovieDBClient() {
        return mTheMovieDBClient;
    }

    public OMDbClient provideOMDbClient() {
        return mOMDbClient;
    }

    private static class SingletonHolder {
        public final static Injection INSTANCE = new Injection();
    }

    public static Injection getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public Picasso providePicasso(Context context){
        return Picasso.with(context);
    }

    private Injection(){
        mOMDbClient = new OMDBClientMock();
        mTheMovieDBClient = new TheMovieDbMock();
    }

    public BaseSchedulerProvider providerSchedulerProvider(){
        return SchedulerProvider.getInstance();
    }
}
