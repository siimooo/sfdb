# SFDb - Search Films Database

*Movies, series.*

[![screen](https://bitbucket.org/siimooo/sfdb/raw/master/screenrecord.gif)]


SFDb is a showcase of a simple Android architecture MVP pattern. It uses several 3rd party libraries such as RxJava, OkHttp.
Application pulls data from several open REST APIs like omdb and the movie db

Big thanks to https://github.com/googlesamples/android-architecture

# Why?

Because everyone loves to watch films, but no one I've tried application give feature to discover films fast and easily.

# Features

* 60 fps all the time.